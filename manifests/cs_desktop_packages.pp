#desktop packages that do not require configuration or dependendency
#    'okular',
class profiles::cs_desktop_packages (
  Array $installed_pkgs = [
    'texlive-latex-recommended',  #C-2001-979
    'latexmk',                    #C-2001-979
    "cmake-qt-gui",
    "joe",                        #C-2005-417
    "git-gui",
    "gedit",
    "meld",
    "libsdl2-mixer-dev",          #I-1912-3831
    "libsdl2-image-dev",          #I-1912-3831
    "libsdl2-ttf-dev",            #I-2001-1169
  ],
  Array $latest_pkgs = [],
#  Array $include_classes = ['google_chrome'],
) {
#Add test to make sure a package is not in more than one of the arrays
  include stdlib
  include profiles::desktop
  ensure_packages ( $installed_pkgs,
    {
      ensure  => 'installed',
      require => Class['profiles::desktop'],
    }
  )
  ensure_packages ( $latest_pkgs,
    {
      ensure  => 'latest',
      require => Class['profiles::desktop'],
    }
  )
#  ensure_resource('class',$include_classes,{'ensure'=>'present'})
# include google_chrome
#moved to foreman config group
}
