#setup repo for atom_editor

class profiles::microsoft_prod::edge (
  $ensure = 'present',
  $edge_pkg = 'microsoft-edge-stable'
) {
  include apt
 
  $_os_facts = $facts['os']
  $_os = downcase($_os_facts['name'])
  $_codename = downcase($_os_facts['distro']['codename'])
  $_release = $_os_facts['release']['major']
  apt::source {'microsoft_edge':
    ensure       => $ensure,
    comment      => 'repository for microsoft edge',
    location     => "https://packages.microsoft.com/repos/edge",
    release      => $_codename,
    repos        => 'stable main',
    architecture => $facts['architecture'],
  }

  package { 'edge':
    ensure => $ensure,
    require => Atp::Source['microsoft_edge'],
  }
}
