#setup repo for atom_editor

class profiles::microsoft_prod::repo (
  $gpgfile = '/var/tmp/microsoft_prod.gpg',
) {
  include apt
 
  file { $gpgfile:
    ensure => 'file',
    source => 'puppet:///modules/profiles/microsoft.gpg'
  }

#  file { '/etc/apt/sources.list.d/visual_studio_code.list':
#    ensure => 'absent'
#  }

  $_os_facts = $facts['os']
  $_os = downcase($_os_facts['name'])
  $_codename = downcase($_os_facts['distro']['codename'])
  $_release = $_os_facts['release']['major']
  apt::source {'microsoft_prod':
    comment      => 'repository for microsoft products',
    location     => "https://packages.microsoft.com/${_os}/${_release}/prod",
    release      => $_codename,
    repos        => 'main',
    architecture => $facts['architecture'],
    key          => {
      id     => 'BC528686B50D79E339D3721CEB3E94ADBE1229CF',
      source => $gpgfile,
    },
    require      => File[$gpgfile], 
  }
}
