#
define profiles::microsoft_prod::package (
  Enum['installed','absent'] $ensure = 'installed',
  String $pkg_name = $title,
  Hash $pkg_options = {},
) {
  include profiles::microsoft_prod::repo
  include stdlib

  $options = merge( {
      ensure  => $ensure,
      require => Apt::Source['microsoft_prod'],
    },
    $pkg_options
  )
  ensure_packages ( [ $pkg_name ], $options )
}
