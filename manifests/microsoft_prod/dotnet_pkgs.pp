#
define profiles::microsoft_prod::dotnet_pkgs (
  Enum['installed','absent'] $ensure,
  String $pkg_prefix,
  Array $installed_versions,
  Array $abent_versions,
) {
  include profiles::microsoft_prod::repo

  $intersect = intersection($installed_versons,$absent_versions)
  unless empty($intersect) {
    $message = template('profiles/ErrAbesntInstalledOverlap.erb')
    fail($message)
  }
  unless empty($installed_version) {
    $pkgs = prefix($installed_versions,$pkg_prefix)
    ensure_packages ( $pkgs, {
      ensure => 'installed',
      require => Apt::Source['microsoft_prod'],
    } )
  }
  unless empty($absent_versions) {
    $pkgs = prefix($absent_versions,$pkg_prefix)
    ensure_packages ( $pkgs, {
      ensure => 'absent',
      require => Apt::Source['microsoft_prod'],
    } )
  }
}
