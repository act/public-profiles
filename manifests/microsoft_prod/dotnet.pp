#
class profiles::microsoft_prod::dotnet (
  Array $installed_runtime = ['2.2'],
  Array $absent_runtime    = [],
  Array $installed_sdk     = [],
  Array $absent_sdk        = [],
) {
  include profiles::microsoft_prod::repo

  profiles::microsoft_prod::dotnet_pkgs { 'runtime':
    installed_versions => $installed_runtime,
    absent_versions => $absent_runtime,
    pkg_prefix => 'dotnet-runtime-',
  }
  profiles::microsoft_prod::dotnet_pkgs { 'sdk':
    installed_versions => $installed_sdk,
    absent_versions => $absent_sdk,
    pkg_prefix => 'dotnet-sdk-',
  }
}
