#
class profiles::desktop {
  case $::facts['os']['family'] {
    'Debian': {
      $desktoppackage = lookup("profiles::desktop::${::desktop_distribution}")
      package { $desktoppackage:
        ensure => 'installed',
      }
    }
    default: { fail("Desktop is not supported on ${::facts['os']['family']}.") }
  }
#notice("desktop_distibution = ${::desktop_distribution}")
#notify{"desktop_distibution = ${::desktop_distribution}":}
#notice("desktop_package = ${desktoppackage}")
#notify{"desktop_package = ${desktoppackage}":}
}
