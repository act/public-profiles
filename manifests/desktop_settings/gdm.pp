#
class profiles::desktop_settings::gdm {
  include profiles::desktop_settings::gnome_background
  file { '/etc/dconf/profile/gdm':
    content => "user-db:user\nsystem-db:gdm\n",
  }
  file { '/etc/dconf/db/gdm.d':
      ensure => 'directory',
      require => File['/etc/dconf/profile/gdm'],
  }
  file { '/etc/dconf/db/gdm.d/10-login-screen':
    content => "# Login manager options
# =====================
[org/gnome/login-screen]
#logo='/usr/share/icons/gnome/48x48/places/debian-swirl.png'
#fallback-logo='/usr/share/icons/gnome/48x48/places/debian-swirl.png'

# - Disable user list
disable-user-list=true
# - Disable restart buttons
# disable-restart-buttons=true
# - Show a login welcome message
banner-message-enable=true
banner-message-text='Login with your University username'
",
    require => File['/etc/dconf/db/gdm.d'],
    notify => Exec['dconf-update'],
  }
  exec { 'dconf-update':
    command => '/usr/bin/dconf update',
  }

  # set new theme with SSEbackdrop 
  file { '/usr/share/gnome-shell/gnome-shell-theme.gresource':
    source => 'puppet:///modules/profiles/desktop_settings/gnome-shell-theme.gresource',
    notify => Exec['dconf-update'],
  }

}
