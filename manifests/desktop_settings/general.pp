#include desktop packages and settings
class profiles::desktop_settings::general {

  case $::facts['os']['family'] {
    'Debian': {
      case $::desktop_distribution {
        'ubuntu','unity':   {
          include profiles::desktop_settings::lightdm
        }
        'xubuntu','xfce':   {
          include profiles::desktop_settings::xfce4
        }
        'mate','cloudtop':   {
          include profiles::desktop_settings::mate
        }
        'gnome':          {
           include profiles::desktop_settings::gdm
        }
        'kubuntu','kde':    {
          if versioncmp($::operatingsystemrelease,'16.04') < 0 {
            file { "/etc/lightdm/lightdm-kde-greeter.conf":
              content => "[greeter]\ntheme-name=classic\n",
              require => Exec[check_lightdm]
            }
            include profiles::desktop_settings::lightdm
          }
        }
        default: {
          fail("Desktop distribution ${::desktop_distribution} is not supported.")
        }
      }

    }
    'RedHat': {
    }
    default:        { fail("Desktop OS ${::facts['os']['family']} is not supported.") }
  }

}

