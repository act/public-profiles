#include desktop packages and settings
class profiles::desktop_settings::gnome_background {
#branding
  $background='/usr/share/backgrounds/SSEBackdrop.png'
  file { $background:
    source => 'puppet:///modules/profiles/desktop_settings/SSEBackdrop.png',
    mode   => '0755',
  }
  $alt_background='/usr/share/backgrounds/AltBackdrop.png'
  file { $alt_background:
    source => 'puppet:///modules/profiles/desktop_settings/AltBackdrop.png',
    mode   => '0755',
  }
  gnome::gsettings { 'desktop-background':
    schema  => 'org.gnome.desktop.background',
    key     => 'picture-uri',
    value   => "'file://$background'",
    require => File[$background],
  }
  gnome::gsettings { 'desktop-background-options':
    schema  => 'org.gnome.desktop.background',
    key     => 'picture-options',
    value   => "'scaled'",
    require => File[$background],
  }
}
