#include desktop packages and settings
class profiles::desktop_settings::mate_screensaver ( 
  $lock_enabled  = 'true'
) {
#branding
  gnome::gsettings { 'mate_screensaver':
    schema  => 'org.mate.screensaver',
    key     => 'lock-enabled',
    value   => $lock_enabled,
  }
  gnome::gsettings { 'mate_screensaver_idle_activation_enabled':
    schema  => 'org.mate.screensaver',
    key     => 'idle-activation-enabled',
    value   => $lock_enabled,
  }

}
