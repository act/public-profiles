#include desktop packages and settings
class profiles::desktop_settings::lightdm {
#branding
  include profiles::lightdm
#  include profiles::desktop_settings::gnome_background
      gnome::gsettings { 'lightdm-draw-grid':
        schema => 'com.canonical.unity-greeter',
        key    => 'draw-grid',
        value  => 'false',
      }
#      $background = $::profiles::desktop_settings::gnome_background::background
      gnome::gsettings { 'lightdm-background':
        schema  => 'com.canonical.unity-greeter',
        key     => 'background',
        value   => "'$::gnome_background'",
        require => File[$::gnome_background],
      }
      gnome::gsettings { 'Unity_Launcher-favorites':
        schema => 'com.canonical.Unity.Launcher',
        key    => 'favorites',
        value  => "['application://ubiquity.desktop', 'application://nautilus.desktop', 'application://google-chrome.desktop', 'application://firefox.desktop', 'unity://running-apps', 'unity://expo-icon', 'unity://devices']",
      }

}
