#change shutdown in ubuntu_mate.layout to logoff
class profiles::desktop_settings::mate_shutdown_logout(
  String $path    = '/usr/share/mate-panel/layouts/ubuntu-mate.layout',
  String $section = 'Object shutdown',
  String $value   = 'logout',
) {
  include profiles::desktop_settings
  ini_setting { "mate_panel_shutdown_logoff":
    path    => $path,
    section => $section,
    setting => 'action-type',
    value   => $value,
    force_new_section_creation => 'no',
    require => Class['profiles::desktop_settings'],
  } 

}
