#include desktop packages and settings
class profiles::desktop_settings::mate {
#include lightdm
  include profiles::lightdm
  include profiles::desktop_settings::gnome_background
#  $background = $::profiles::desktop_settings::gnome_background::background
#  $default_background = '/usr/share//xfce4/backdrops/xubuntu-wallpaper.png'
#  file { $default_background:
#    ensure => 'link',
#    target => $background,
#  }
  gnome::gsettings { 'mate-background':
    priority => 'uor',
    schema   => 'org.mate.background',
    key      => 'picture-filename',
    value    => "'$::gnome_background'",
    require  => File[$::gnome_background],
  }
  #keybaord layouts
  gnome::gsettings { 'mate-kbd-layouts':
    priority => 'uor',
    schema   => 'org.mate.peripherals-keyboard-xkb.kbd',
    key      => 'layouts',
    value    => '[\'gb\']',
  }

#  file_line { 'lightdm-gtk-background':
#    path => '/etc/lightdm/lightdm-gtk-greeter.conf',
#    match => '^background',
#    line  => "background=${background}",
#    after => "^[greeter]$",
#  }
}
