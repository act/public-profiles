#include desktop packages and settings
class profiles::desktop_settings::mate_caja_disabled_extensions ( 
  $extensions  = '[]'
) {
#branding
  gnome::gsettings { 'mate_caja_disabled_extensions':
    schema  => 'org.mate.caja.extensions',
    key     => 'disabled-extensions',
    value   => $extensions,
  }

}
