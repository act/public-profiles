#setup repo for atom_editor

class profiles::visual_studio_code::repo (
  $gpgfile = '/var/tmp/microsoft.gpg',
) {
  include apt
 
  file { $gpgfile:
    ensure => 'file',
    source => 'puppet:///modules/profiles/microsoft.gpg'
  }

  file { '/etc/apt/sources.list.d/visual_studio_code.list':
    ensure => 'absent'
  }

  apt::source {'vscode':
    comment      => 'repository for visual studio code editor',
    location     => 'https://packages.microsoft.com/repos/vscode',
    release      => 'stable',
    architecture => $facts['architecture'],
    key          => {
      id     => 'BC528686B50D79E339D3721CEB3E94ADBE1229CF',
      source => $gpgfile,
    },
    require      => File[$gpgfile], 
  }
}
