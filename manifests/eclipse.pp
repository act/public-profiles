#Install eclipse and plugins
class profiles::eclipse (
  String $method = 'package',
  String $ensure = 'installed',
) {
  class { 'eclipse':
    method => 'package'
  }
  $plugin_method = $method ? {
    'download' => 'p2_director',
    default    => $method,
  }
  
  eclipse::plugin { 'egit':
    method => $plugin_method,
    iu     => 'org.eclipse.egit.feature.group'
  }
}
