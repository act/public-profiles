#install atom_editor

class profiles::visual_studio_code (
  String $ensure = 'installed',
){

  include apt
  include profiles::desktop
  include profiles::apt_transport_https
  include profiles::visual_studio_code::repo

  package { 'code':
    ensure  => $ensure,
    require => [
      Class['profiles::desktop'],
      Class['profiles::apt_transport_https'],
      Class['profiles::visual_studio_code::repo'],
      Class['apt::update'],
    ],
  }
}
