#define type for anaconda environment_modules env

define profiles::environment_modules::anaconda_env (
  $version     = $title,
  $moduledir = lookup('profiles::environment_modules::module_dir'),
  $ensure    = 'file',
) {
  include profiles::environment_modules::anaconda
 
    $install_dir = '/opt/anaconda'

  exec { "anaconda/${version}":
    command => 'true',
    path    => ['/bin', '/usr/bin'],
    onlyif  => "test -e /opt/anaconda/envs/${version}/bin/python"
  }

  file { "${moduledir}/anaconda/${version}":
    ensure  => $ensure,
    content => template('profiles/environment_modules/anaconda_env.erb'),
    require => [
      Exec["anaconda/${version}"],
      File["${moduledir}/anaconda"]
    ],
  }
}
