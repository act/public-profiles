#environment modeule for anaconda

class profiles::environment_modules::anaconda (
  $moduledir = lookup('profiles::environment_modules::module_dir'),
) {
require profiles::environment_modules

  $install_dir = '/opt/anaconda'
  $module_default = 'root'
  $version = $module_default
  file { "${moduledir}/anaconda":
    ensure => directory,
  }

  file { "${moduledir}/anaconda/.version":
    ensure  => file,
    content => template('profiles/environment_modules/module_version.erb'),
    require => File["${moduledir}/anaconda"],
  }
  file { "${moduledir}/anaconda/${module_default}":
    ensure  => file,
    content => template('profiles/environment_modules/anaconda.erb'),
    require => File["${moduledir}/anaconda"],
  }

}
