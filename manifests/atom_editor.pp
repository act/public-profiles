#install atom_editor

class profiles::atom_editor (
  String $ensure = 'installed',
){

  include apt
  include profiles::desktop
  include profiles::apt_transport_https
  include profiles::atom_editor::repo

  package { 'atom':
    ensure => $ensure,
    require => [
      Class['profiles::desktop'],
      Class['profiles::apt_transport_https'],
      Class['profiles::atom_editor::repo'],
      Class['apt::update'],
    ],
  }
  ensure_packages ( ['hunspell-en-gb'], {'ensure' => $ensure } )

}
