#
class profiles::nano (
  String $ensure = 'installed',
  Array $pkgs = ['nano'],
) {
  package { $pkgs: 
    ensure => $ensure,
  }
}
