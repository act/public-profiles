#make sure environment modules packages is installed

class profiles::environment_modules (
  $ensure = 'installed',
  $pkg_name = ['environment-modules','tcl'],
) {
  unless empty($pkg_name) {
    ensure_packages( $pkg_name, { ensure => 'present', })
    file { "/usr/bin/add.modules":
      ensure  => file,
      source  => 'puppet:///modules/profiles/environment_modules/add.modules',
      mode    => '755',
      owner   => 'root',
      group   => 'root',
    }
  }
}
