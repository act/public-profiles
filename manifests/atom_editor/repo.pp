#setup repo for atom_editor

class profiles::atom_editor::repo () {
  include apt
 
  apt::source {'atom':
    comment      => 'repository for atom editor',
    location     => 'https://packagecloud.io/AtomEditor/atom/any/',
    release      => 'any',
    architecture => $facts['architecture'],
    key          => {
      id     => '0A0FAB860D48560332EFB581B75442BBDE9E3B09',
#      server => 'packagecloud.io',
      source => 'https://packagecloud.io/AtomEditor/atom/gpgkey',
    }
  }
}
