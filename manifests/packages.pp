#packages that do not require configuration or dependendency
class profiles::packages (
  Array $installed = [],
  Array $latest = [],
  Array $absent = [],
) {
#Add test to make sure a package is not in more than one of the arrays
  ensure_packages ( $installed, {'ensure' => 'installed', } )
  ensure_packages ( $latest,    {'ensure' => 'latest', } )
  ensure_packages ( $absent,    {'ensure' => 'absent', } )
}
