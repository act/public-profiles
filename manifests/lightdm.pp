#
class profiles::lightdm
(
  String  $pkg_enabled     = 'installed',
  String  $svc_enabled     = 'true',
  String  $svc_ensure      = 'running',
  String  $config_dir      = '/etc/lightdm/lightdm.conf.d',
  Boolean $use_config_dir  = true,
  String  $config_file     = '/etc/lightdm/lightdm.conf',
  String  $greeter_file    = '/etc/lightdm/slick-greeter.conf',
  String  $greeter_section = 'Greeter',
) {
  include stdlib

  package { 'lightdm':
    ensure => $pkg_enabled,
  }

  service { 'lightdm':
    ensure   => $svc_ensure,
  #  enable   => $svc_enabled,
  #  provider => 'systemd',
  }

  if $use_config_dir {
#    file { "${config_dir}/50-no-guest.conf":
#      content => "[Seat:*]\nallow-guest=false\n",
#      require => Package['lightdm'],
#      notify  => Service['lightdm'],
#    }
#    file { "${config_dir}/50-hide-users.conf":
#      content => "[SeatDefaults]\ngreeter-hide-users=true\ngreeter-show-manual-login=true\n",
#      require => Package['lightdm'],
#      notify  => Service['lightdm'],
#    }
    file { $greeter_file:
      content => "[${greeter_section}]\nbackground=${::gnome_background}\n",
      require => Package['lightdm'],
      notify  => Service['lightdm'],
    }

  } else {
    file { '/etc/lightdm/lightdm.conf':
      ensure  => 'present',
      require => Package['lightdm'],
    }
    file_line { 'lightdm-seat-defaults':
      line    => '[SeatDefaults]',
      path    => '/etc/lightdm/lightdm.conf',
      require => File['/etc/lightdm/lightdm.conf'],
      notify  => Service['lightdm'],
    }
#    file_line { 'lightdm-hide-users':
#      line    => 'greeter-hide-users=true',
#      path    => '/etc/lightdm/lightdm.conf',
#      require => File_line[lightdm-seat-defaults],
#      notify  => Service['lightdm'],
#    }
#    file_line { 'lightdm-allow-guest':
#      line    => 'greeter-allow-guest=false',
#      path    => '/etc/lightdm/lightdm.conf',
#      require => File_line[lightdm-seat-defaults],
#      notify  => Service['lightdm'],
#    }
#    file_line { 'lightdm-show-manual-login':
#      line    => 'greeter-show-manual-login=true',
#      path    => '/etc/lightdm/lightdm.conf',
#      require => File_line[lightdm-seat-defaults],
#      notify  => Service['lightdm'],
#    }

  }
}
