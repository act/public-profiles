#desktop packages that do not require configuration or dependendency
#    'okular',
class profiles::met_desktop_packages (
  Array $installed_pkgs = [
    'cmake',
    'geany',           #I-2004-3217
    'ghostscript',
    'gimp',
    'git',
    'gv', 
    'imagemagick',
    'latexdiff',
    'latex2rtf',
    'libreoffice',
    'lynx',
    'mpage',
    'pdftk',
    'php',
    'r-base',
    'rcs',
    'subversion',
    'texlive-full',
    'texmaker',
    'w3m',
    'vlc',
    'xfig',
    'xournal'
  ],
  Array $latest_pkgs = [],
#  Array $include_classes = ['google_chrome'],
) {
#Add test to make sure a package is not in more than one of the arrays
  include stdlib
  include profiles::desktop
  ensure_packages ( $installed_pkgs,
    {
      ensure  => 'installed',
      require => Class['profiles::desktop'],
    }
  )
  ensure_packages ( $latest_pkgs,
    {
      ensure  => 'latest',
      require => Class['profiles::desktop'],
    }
  )
#  ensure_resource('class',$include_classes,{'ensure'=>'present'})
# include google_chrome
#moved to foreman config group
}
