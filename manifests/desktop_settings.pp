#
class profiles::desktop_settings {
  include profiles::desktop
  include profiles::desktop_settings::general
  Class['profiles::desktop'] -> Class['profiles::desktop_settings::general']

}
