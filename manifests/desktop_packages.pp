#desktop packages that do not require configuration or dependendency
class profiles::desktop_packages (
  Array $installed = [],
  Array $latest = [],
  Array $absent = [],
) {
#Add test to make sure a package is not in more than one of the arrays
  include profiles::desktop
  ensure_packages ( $installed,
    {
      ensure  => 'installed',
      require => Class['profiles::desktop'],
    }
  )
  ensure_packages ( $latest,
    {
      ensure  => 'latest',
      require => Class['profiles::desktop'],
    }
  )
  ensure_packages ( $absent,
    {
      ensure  => 'absent',
      require => Class['profiles::desktop'],
    }
  )
}
